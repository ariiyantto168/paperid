<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Rekeningku extends Model
{
    use SoftDeletes;

    protected $table = 'rekeningku';
    protected $primaryKey = 'id_rekeningku';

    protected $fillable = [
        'id_users','id_finance_accounts','nama_akun','nama_bank','nomor_rekening','saldo','keterangan',
    ];
}
