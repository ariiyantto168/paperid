<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Uangmasuk extends Model
{
    use SoftDeletes;

    public $timestamps = false;

    protected $table = 'uang_masuk';
    protected $primaryKey = 'id_uang_masuk';

    protected $fillable = [
        'id_users',
        'akun_tujuan',
        'kategori_pemasukan',
        'saldo_sekarang',
        'saldo_sebelumnya',
        'jumlah',
        'tanggal',
        'jam',
        'keterangan'
    ];
}
