<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Financeaccounts extends Model
{
    use SoftDeletes;

    protected $table = 'finance_accounts';
    protected $primaryKey = 'id_finance_accounts';

    protected $fillable = [
        'saldo',
    ];
}
