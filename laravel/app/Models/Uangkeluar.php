<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Uangkeluar extends Model
{
    use SoftDeletes;
    public $timestamps = false;

    protected $table = 'uang_keluar';
    protected $primaryKey = 'id_uang_keluar';

    protected $fillable = [
        'id_users',
        'akun_tujuan',
        'kategori_pengeluaran',
        'saldo_sekarang',
        'saldo_sebelumnya',
        'jumlah',
        'tanggal',
        'jam',
        'keterangan'
    ];
}
