<?php

namespace App\Http\Controllers;

use JWTAuth;
use Validator;
use App\Models\User;
use App\Models\Financeaccounts;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Tymon\JWTAuth\Exceptions\JWTException;


use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function login(Request $request)
    {
        $credentials = $request->only('username', 'password');
        $user = User::where('username', $credentials['username'])->first();
            
        if (!empty($user)) {                
            try {
                if (! $token = JWTAuth::attempt($credentials)) {
                    return response()->json(['error' => 'invalid_credentials'], 400);
                }
            } catch (JWTException $e) {
                return response()->json(['error' => 'could_not_create_token'], 500);
            }

            $token = compact('token');
            $user->token = $token['token'];
            $user->last_login = date('Y-m-d H:i:s');
            $user->save();


            return response()->json([
                'code' => 200,
                'messages' => 'success',
                'user' => $user
            ]);

            }else {
                return response()->json([
                    'code' => 400,
                    'messages' => 'username not found'
                ]);            }   
    }

    public function profile()
    {
        try {

            if (! $user = JWTAuth::parseToken()->authenticate()) {
                return response()->json(['user_not_found'], 404);
            }

        } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {

            return response()->json(['token_expired'], $e->getStatusCode());

        } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {

            return response()->json(['token_invalid'], $e->getStatusCode());

        } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {

            return response()->json(['token_absent'], $e->getStatusCode());

        }

        return response()->json([
            'code'      => 200,
            'message'   => 'success',
            'user'      => User::with(['finance'])
                            ->where('id_users',
                            compact('user')['user']->id_users)
                            ->first(),
        ]);
    }

    public function logout(Request $request) {
        Auth::logout();
        return response()->json(['message' => 'successfully signed out']);
    }

    public function register(Request $request)
    {
        $table = [
            'nama' => 'required',
            'username' => 'required|unique:users',
            'password' => 'required|min:6',
            'email'    => 'required|email|unique:users',
        ];

        $messages = [
            'nama.required' => 'Nama wajib diisi.',
            'password.required' => 'password wajib diisi.',
            'username.required' => 'User name wajib diisi.',
            'username.unique'      => 'User name sudah tersedia.',
            'email.unique'      => 'Email sudah tersedia.',
            'email.required' => 'Email wajib diisi.',
        ];

        $validator = Validator::make($request->all(), $table, $messages);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }


        $user = new User;
        $user->nama = $request->nama;
        $user->role = 'accounts';
        $user->email = $request->email;
        $user->username = $request->username;
        $user->is_active = true;
        $user->password = Hash::make($request->password);
        $user->save();

        $account = new Financeaccounts;
        $account->id_users = $user->id_users;
        $account->saldo = 0;
        $account->save();

        return response()->json([
            'code'     => 200,
            'messages' => 'success',
            'user'     => $user
        ]);

    }

}
