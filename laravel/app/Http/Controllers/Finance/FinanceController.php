<?php

namespace App\Http\Controllers\Finance;

use JWTAuth;
use Validator;
use Carbon\Carbon;
use App\Models\User;
use App\Models\Topup;
use App\Models\Uangmasuk;
use App\Models\Uangkeluar;
use App\Models\Rekeningku;
use App\Models\Pengeluaran;
use App\Models\Pemasukan;
use Illuminate\Http\Request;
use App\Models\Financeaccounts;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;

class FinanceController extends Controller
{


    public function create_rekeningku(Request $request)
    {
        try {
            $user = User::with(['finance'])->where('id_users',Auth::user()->id_users)->first();

            $rekeningku = Rekeningku::create([
                'id_users'              => $user->id_users,
                'id_finance_accounts'   => $user->finance->id_finance_accounts,
                'nama_akun'             => $request->nama_akun,
                'nama_bank'             => $request->nama_bank,
                'nomor_rekening'        => $request->nomor_rekening,
                'keterangan'            => $request->keterangan,
                'saldo'                 => $request->saldo,
            ]);

            $user->finance->update([
                'saldo' => $user->finance->saldo += $rekeningku->saldo
            ]);


            return response()->json([
                'code'    => 200,
                'message' => 'success',
                'rekeningku'   => $rekeningku
            ]);

        } catch (\Throwable $th) {
            return response()->json([
                'success'   => false,
                'msg'       => 'Something Wrong',
                'error'     => $th->getMessage()
            ],400);
        }
    }

    public function filter_uang_masuk(Request $request)
    {
        try {
            return response()->json([
                'code'      =>  200,
                'messages'  =>  'succes',
                'uangMasuk' => Uangmasuk::orWhere("akun_tujuan","like","%".$request->title."%")
                                    ->orWhere("kategori_pemasukan","like","%".$request->title."%")
                                    ->where('id_users',Auth::user()->id_users)
                                    ->paginate(10),
        ]);
        } catch (\Throwable $th) {
            return response()->json([
                'success'   => false,
                'msg'       => 'Something Wrong',
                'error'     => $th->getMessage()
            ],400);
        }

    }

    public function laporan_uang_masuk(Request $request)
    {
        try {
            return response()->json([
                'code'       =>  200,
                'messages'   =>  'succes',
                'uangMasuk' =>  Uangmasuk::where('id_users',Auth::user()->id_users)
                                        ->where('tanggal', '>=', $request->startdate)
                                        ->where('tanggal', '<=', $request->enddate)
                                        ->paginate(10),
            ],200);
        } catch (\Throwable $th) {
            return response()->json([
                'success'   => false,
                'msg'       => 'Something Wrong',
                'error'     => $th->getMessage()
            ],400);
        }
    }

    public function create_uang_masuk(Request $request)
    {
        try {
            if (empty(in_array($request->kategori_pemasukan, $this->pemasukan()))) return response()->json(['code'  =>  400,'message'   => 'data kategori tidak tersedia']);
            if (empty(in_array($request->akun_tujuan, $this->rekeningku()))) return response()->json(['code'  =>  401,'message'   => 'nomor rekening tidak tersedia']);

            $rekeningku = Rekeningku::where('nomor_rekening',$request->akun_tujuan)->first();
            $user = User::with(['finance'])->where('id_users',Auth::user()->id_users)->first();
           

            $uangMasuk = Uangmasuk::create([
                'id_users'              =>  $user->id_users,
                'akun_tujuan'           =>  $request->akun_tujuan,
                'kategori_pemasukan'    =>  $request->kategori_pemasukan,
                'saldo_sebelumnya'      =>  $rekeningku->saldo,
                'saldo_sekarang'        =>  $rekeningku->saldo_sebelumnya += $request->jumlah,
                'jumlah'                =>  $request->jumlah,
                'tanggal'               =>  $request->tanggal,
                'jam'                   =>  $request->jam,
                'keterangan'            =>  $request->keterangan
            ]);

            $user->finance->update([
                'saldo' => $user->finance->saldo += $uangMasuk->jumlah
            ]);

            return response()->json([
                'code'    => 200,
                'message' => 'success',
                'uangMasuk'   => $uangMasuk
            ]);

        } catch (\Throwable $th) {
            return response()->json([
                'success'   => false,
                'msg'       => 'Something Wrong',
                'error'     => $th->getMessage()
            ],400);
        }
    }

    public function get_uang_masuk($id)
    {
        try {
            if (empty(Uangmasuk::where('id_uang_masuk',$id)->first())) {
                return response()->json([
                    'code'    => 400,
                    'message' => 'data tidak tersedia',
                ]);
            }

            return response()->json([
                'code'    => 200,
                'message' => 'success',
                'uangMasuk'   => Uangmasuk::where('id_uang_masuk',$id)->first(),
            ]);

        } catch (\Throwable $th) {
            return response()->json([
                'success'   => false,
                'msg'       => 'Something Wrong',
                'error'     => $th->getMessage()
            ],400);
        }
    }

    public function delete_uang_masuk($id)
    {
        try {
            $uangMasuk = Uangmasuk::where('id_uang_masuk',$id)->first();
            $user = User::with(['finance'])->where('id_users',Auth::user()->id_users)->first();
            
            if (empty($uangMasuk)) {
                return response()->json([
                    'code'      => 400,
                    'message'   => 'notfound'
                ]);
            }

            $user->finance->update([
                'saldo' => $user->finance->saldo -= $uangMasuk->jumlah
            ]);

            return response()->json([
                'code'      => 200,
                'message'   => 'delete success',
                'uangMasuk' =>  Uangmasuk::where('id_uang_masuk',$id)->delete()
            ]);
        } catch (\Throwable $th) {
            return response()->json([
                'success'   => false,
                'msg'       => 'Something Wrong',
                'error'     => $th->getMessage()
            ],400);
        }
    }

    public function update_uang_masuk($id,Request $request)
    {
        try {
            $uangMasuk = Uangmasuk::where('id_uang_masuk',$id)->first();
            $rekeningku = Rekeningku::where('nomor_rekening',$uangMasuk->akun_tujuan)->first();
            $user = User::with(['finance'])->where('id_users',Auth::user()->id_users)->first();
           
            if (empty(in_array($request->kategori_pemasukan, $this->pemasukan()))) return response()->json(['code'  =>  400,'message'   => 'data kategori tidak tersedia']);
            if (empty(in_array($request->akun_tujuan, $this->rekeningku()))) return response()->json(['code'  =>  401,'message'   => 'nomor rekening tidak tersedia']);


            $rekeningku->update([
                'saldo'     =>  $rekeningku->saldo -= $uangMasuk->jumlah
            ]);

            $user->finance->update([
                'saldo' => $user->finance->saldo -= $uangMasuk->jumlah
            ]);

            $uangMasuk->update([
                'id_users'              =>  $user->id_users,
                'akun_tujuan'           =>  $request->akun_tujuan,
                'kategori_pemasukan'    =>  $request->kategori_pemasukan,
                'saldo_sebelumnya'      =>  $rekeningku->saldo,
                'saldo_sekarang'        =>  $rekeningku->saldo_sebelumnya += $request->jumlah,
                'jumlah'                =>  $request->jumlah,
                'tanggal'               =>  $request->tanggal,
                'jam'                   =>  $request->jam,
                'keterangan'            =>  $request->keterangan
            ]);

            $user->finance->update([
                'saldo' => $user->finance->saldo += $uangMasuk->jumlah
            ]);

            return response()->json([
                'code'    => 200,
                'message' => 'success',
                'uangMasuk'   => $uangMasuk
            ]);
            
        } catch (\Throwable $th) {
            return response()->json([
                'success'   => false,
                'msg'       => 'Something Wrong',
                'error'     => $th->getMessage()
            ],400);
        }
    }

    public function filter_uang_keluar(Request $request)
    {
        try {
            return response()->json([
                'code'       =>  200,
                'messages'   =>  'succes',
                'uangKeluar' =>  Uangkeluar::orWhere("akun_tujuan","like","%".$request->title."%")
                                    ->orWhere("kategori_pengeluaran","like","%".$request->title."%")
                                    ->where('id_users',Auth::user()->id_users)
                                    ->paginate(10),
            ],200); 
        } catch (\Throwable $th) {
            return response()->json([
                'success'   => false,
                'msg'       => 'Something Wrong',
                'error'     => $th->getMessage()
            ],400);
        }
    }

    public function laporan_uang_keluar(Request $request)
    {
        try {
            return response()->json([
                'code'       =>  200,
                'messages'   =>  'succes',
                'uangKeluar' =>  Uangkeluar::where('id_users',Auth::user()->id_users)
                                        ->where('tanggal', '>=', $request->startdate)
                                        ->where('tanggal', '<=', $request->enddate)
                                        ->paginate(10),
            ],200);
        } catch (\Throwable $th) {
            return response()->json([
                'success'   => false,
                'msg'       => 'Something Wrong',
                'error'     => $th->getMessage()
            ],400);
        }

    }

    public function create_uang_keluar(Request $request)
    {
        try {
            if (empty(in_array($request->akun_tujuan, $this->rekeningku()))) return response()->json(['code'=>401,'message'=> 'nomor rekening tidak tersedia']);
            if (empty(in_array($request->kategori_pengeluaran, $this->pengeluaran()))) return response()->json(['code'  =>  400,'message'   => 'data kategori tidak tersedia']);
            
            $rekeningku = Rekeningku::where('nomor_rekening',$request->akun_tujuan)->first();
            $user = User::with(['finance'])->where('id_users',Auth::user()->id_users)->first();

            $uangKeluar = Uangkeluar::create([
                'id_users'              =>  $user->id_users,
                'akun_tujuan'           =>  $request->akun_tujuan,
                'kategori_pengeluaran'  =>  $request->kategori_pengeluaran,
                'saldo_sebelumnya'      =>  $rekeningku->saldo,
                'saldo_sekarang'        =>  $rekeningku->saldo_sebelumnya -= $request->jumlah,
                'jumlah'                =>  $request->jumlah,
                'tanggal'               =>  $request->tanggal,
                'jam'                   =>  $request->jam,
                'keterangan'            =>  $request->keterangan
            ]);

            $user->finance->update([
                'saldo' => $user->finance->saldo -= $uangKeluar->jumlah
            ]);

            return response()->json([
                'code'    => 200,
                'message' => 'success',
                'uangKeluar'   => $uangKeluar
            ]);
        } catch (\Throwable $th) {
            return response()->json([
                'success'   => false,
                'msg'       => 'Something Wrong',
                'error'     => $th->getMessage()
            ],400);
        }
    }

    public function get_uang_keluar($id)
    {
        try {
            if (empty(Uangkeluar::where('id_uang_keluar',$id)->first())) {
                return response()->json([
                    'code'    => 400,
                    'message' => 'data tidak tersedia',
                ]);
            }

            return response()->json([
                'code'    => 200,
                'message' => 'success',
                'uangKeluar'   => Uangkeluar::where('id_uang_keluar',$id)->first(),
            ]);

        } catch (\Throwable $th) {
            return response()->json([
                'success'   => false,
                'msg'       => 'Something Wrong',
                'error'     => $th->getMessage()
            ],400);
        }
    }

    public function update_uang_keluar($id,Request $request)
    {
        $uangKeluar = Uangkeluar::where('id_uang_keluar',$id)->first();
        $rekeningku = Rekeningku::where('nomor_rekening',$uangKeluar->akun_tujuan)->first();
        $user = User::with(['finance'])->where('id_users',Auth::user()->id_users)->first();
           
        if (empty(in_array($request->kategori_pengeluaran, $this->pengeluaran()))) return response()->json(['code'  =>  400,'message'   => 'data kategori tidak tersedia']);
        if (empty(in_array($request->akun_tujuan, $this->rekeningku()))) return response()->json(['code'  =>  401,'message'   => 'nomor rekening tidak tersedia']);

        $rekeningku->update([
            'saldo'     =>  $rekeningku->saldo += $uangKeluar->jumlah
        ]);

        $user->finance->update([
            'saldo' => $user->finance->saldo += $uangKeluar->jumlah
        ]);

        $uangKeluar = Uangkeluar::create([
            'id_users'              =>  $user->id_users,
            'akun_tujuan'           =>  $request->akun_tujuan,
            'kategori_pengeluaran'  =>  $request->kategori_pengeluaran,
            'saldo_sebelumnya'      =>  $rekeningku->saldo,
            'saldo_sekarang'        =>  $rekeningku->saldo_sebelumnya -= $request->jumlah,
            'jumlah'                =>  $request->jumlah,
            'tanggal'               =>  $request->tanggal,
            'jam'                   =>  $request->jam,
            'keterangan'            =>  $request->keterangan
        ]);

        $user->finance->update([
            'saldo' => $user->finance->saldo -= $uangKeluar->jumlah
        ]);

        return response()->json([
            'code'    => 200,
            'message' => 'success',
            'uangKeluar'   => $uangKeluar
        ]);

    }

    public function delete_uang_keluar($id)
    {
        try {
            $uangKeluar = Uangkeluar::where('id_uang_keluar',$id)->first();
            $user = User::with(['finance'])->where('id_users',Auth::user()->id_users)->first();
            
            if (empty($uangKeluar)) {
                return response()->json([
                    'code'      => 400,
                    'message'   => 'notfound'
                ]);
            }

            $user->finance->update([
                'saldo' => $user->finance->saldo -= $uangKeluar->jumlah
            ]);

            return response()->json([
                'code'      => 200,
                'message'   => 'delete success',
                'uangKeluar' =>  Uangkeluar::where('id_uang_keluar',$id)->delete()
            ]);
        } catch (\Throwable $th) {
            return response()->json([
                'success'   => false,
                'msg'       => 'Something Wrong',
                'error'     => $th->getMessage()
            ],400);
        }
    }

    protected function pemasukan()
    {
        $uangs = Pemasukan::all(['kategori_pemasukan']); 
        $array = [];      
        foreach ($uangs as $obj)
        {
            $datas = $obj->kategori_pemasukan;
            array_push($array, $datas);
        }
        return $array;
    }

    protected function pengeluaran()
    {
        $uangs = Pengeluaran::all(['kategori_pengeluaran']); 
        $array = [];      
        foreach ($uangs as $obj)
        {
            $datas = $obj->kategori_pengeluaran;
            array_push($array, $datas);
        }
        return $array;
    }

    protected function rekeningku()
    {
        $rekeningku = Rekeningku::all(['nomor_rekening']);

        $array = [];      
        foreach ($rekeningku as $obj)
        {
            $datas = $obj->nomor_rekening;
            array_push($array, $datas);
        }
        return $array;
        
    }
}
