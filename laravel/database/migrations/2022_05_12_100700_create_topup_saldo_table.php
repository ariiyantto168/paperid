<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTopupSaldoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('topup_saldo', function (Blueprint $table) {
            $table->increments('id_topup_saldo');
            $table->integer('id_finance_accounts');
            $table->integer('id_users');
            $table->integer('saldo');
            $table->string('topup_by');
            $table->SoftDeletes();
            $table->dateTime('created_at')->nullable();
            $table->dateTime('updated_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('topup_saldo');
    }
}
