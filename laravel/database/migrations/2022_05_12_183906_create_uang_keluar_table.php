<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUangKeluarTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('uang_keluar', function (Blueprint $table) {
            $table->increments('id_uang_keluar');
            $table->integer('id_users');
            $table->string('akun_tujuan');
            $table->string('kategori_pengeluaran');
            $table->integer('saldo_sekarang');
            $table->integer('saldo_sebelumnya');
            $table->integer('jumlah');
            $table->date('tanggal');
            $table->string('jam');
            $table->text('keterangan')->nullable();
            $table->SoftDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('uang_keluar');
    }
}
