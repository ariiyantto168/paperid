<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRekeningkuTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rekeningku', function (Blueprint $table) {
            $table->increments('id_rekeningku');
            $table->integer('id_finance_accounts');
            $table->integer('id_users');
            $table->string('nama_akun');
            $table->string('nama_bank');
            $table->string('nomor_rekening');
            $table->string('saldo');
            $table->text('keterangan')->nullable();
            $table->SoftDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rekeningku');
    }
}
