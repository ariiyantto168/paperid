<?php

use Illuminate\Database\Seeder;
use App\Models\KategoriPengeluaran;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;

class PengeluaranSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $dbkategori = [
            [
               'kategori_pengeluaran' => 'pajak',
               'logo' => 'images1.jpg',
            ],
            [
                'kategori_pengeluaran' => 'sedekah atau donasi',
                'logo' => 'images2.jpg',
             ],
             [
                'kategori_pengeluaran' => 'asuransi',
                'logo' => 'images3.jpg',
             ],
             [
                'kategori_pengeluaran' => 'cicilan atau hutang',
                'logo' => 'images4.jpg',
             ],
             [
                'kategori_pengeluaran' => 'anak',
                'logo' => 'images5.jpg',
             ],
             [
                'kategori_pengeluaran' => 'pendidikan',
                'logo' => 'images6.jpg',
             ],
             [
                'kategori_pengeluaran' => 'tagihan bulanan',
                'logo' => 'images6.jpg',
             ],
             [
                'kategori_pengeluaran' => 'belanja bulanan',
                'logo' => 'images6.jpg',
             ],
             [
                'kategori_pengeluaran' => 'pengeluaran pribadi',
                'logo' => 'images6.jpg',
             ],
             [
                'kategori_pengeluaran' => 'kendaraan',
                'logo' => 'images6.jpg',
             ],
             [
                'kategori_pengeluaran' => 'kesehatan',
                'logo' => 'images6.jpg',
             ],
             [
                'kategori_pengeluaran' => 'liburan',
                'logo' => 'images6.jpg',
             ],
             [
                'kategori_pengeluaran' => 'hobi',
                'logo' => 'images6.jpg',
             ],
             [
                'kategori_pengeluaran' => 'biaya lainnya',
                'logo' => 'images6.jpg',
             ],
            
       ];

       DB::table('kategori_pengeluaran')->insert($dbkategori);
    }
}
