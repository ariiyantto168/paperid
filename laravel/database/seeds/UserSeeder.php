<?php

use Illuminate\Database\Seeder;
use App\Models\User;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;



class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $dbusers = [
            [
               'nama' => 'admin',
               'username' => 'admin',
               'role' => 'admin',
               'email' => 'admin@testing.com',
               'is_active' => true,
               'password' =>  Hash::make('admin123'),
            ],
            [
                'nama' => 'accounts',
                'username' => 'accounts',
                'role' => 'accounts',
                'email' => 'accounts@testing.com',
                'is_active' => true,
                'password' =>  Hash::make('accounts123'),
             ]
       ];

       DB::table('users')->insert($dbusers);

       $dbfinance = [
           [
                'id_users'  =>  2,
                'saldo'    =>  0
           ]
       ];

       DB::table('finance_accounts')->insert($dbfinance);

    }
}
