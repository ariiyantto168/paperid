<?php

use Illuminate\Database\Seeder;
use App\Models\KategoriPemasukan;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;

class PemasukanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $dbkategori = [
            [
               'kategori_pemasukan' => 'gaji',
               'logo' => 'images1.jpg',
            ],
            [
                'kategori_pemasukan' => 'bonus dan tunjangan',
                'logo' => 'images2.jpg',
             ],
             [
                'kategori_pemasukan' => 'pendapatan pasif atau bisnis',
                'logo' => 'images3.jpg',
             ],
             [
                'kategori_pemasukan' => 'pendapatan lainnya',
                'logo' => 'images4.jpg',
             ],
             [
                'kategori_pemasukan' => 'bunga atau investasi',
                'logo' => 'images5.jpg',
             ],
            
       ];

       DB::table('kategori_pemasukan')->insert($dbkategori);
    }
}
