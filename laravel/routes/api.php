<?php

use Illuminate\Http\Request;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\Finance\FinanceController;


Route::post('login', [AuthController::class, 'login']);
Route::post('register', [AuthController::class, 'register']);


Route::group(['middleware' => ['jwt.verify']], function () {
    Route::get('profile', [AuthController::class, 'profile']);
    Route::post('logout', [AuthController::class, 'logout']);
    // rekeningku
    Route::post('rekeningku', [FinanceController::class, 'create_rekeningku']);
    // uang masuk
    Route::get('uang-masuk', [FinanceController::class, 'filter_uang_masuk']);
    Route::get('uang-masuk', [FinanceController::class, 'laporan_uang_masuk']);
    Route::post('uang-masuk', [FinanceController::class, 'create_uang_masuk']);
    Route::delete('uang-masuk/{id}', [FinanceController::class, 'delete_uang_masuk']);
    Route::get('uang-masuk/{id}', [FinanceController::class, 'get_uang_masuk']);
    Route::post('uang-masuk/{id}', [FinanceController::class, 'update_uang_masuk']);
    // uang keluar
    Route::get('uang-keluar', [FinanceController::class, 'filter_uang_keluar']);
    Route::get('uang-keluar', [FinanceController::class, 'laporan_uang_keluar']);
    Route::post('uang-keluar', [FinanceController::class, 'create_uang_keluar']);
    Route::get('uang-keluar/{id}', [FinanceController::class, 'get_uang_keluar']);
    Route::post('uang-keluar/{id}', [FinanceController::class, 'update_uang_keluar']);
    Route::delete('uang-keluar/{id}', [FinanceController::class, 'delete_uang_keluar']);

});

